window.onload = function () {

  myTd = document.querySelector('#my-td');
  myContents = document.querySelectorAll('.my-content');
  myStudyOpenList = document.querySelectorAll('.my-study-open');
  myStudyCloseList = document.querySelectorAll('.my-study-close');

  for (let myContent of myContents) {
    myContent.addEventListener('mouseover', function(){
      myTd.innerHTML =
        `<span class="my-td-span">${myContent.innerText}</span>`;
    });

    myContent.addEventListener('mouseout', function(){
      myTd.innerHTML = null;
    });
  }


  for (let myStudyClose of myStudyCloseList) {
    myStudyClose.addEventListener('click', function(event){
      event.preventDefault();

      let myDetailList = myStudyClose
        .closest('td')
        .querySelectorAll('.my-study-detail');

      let myOpenList = myStudyClose
        .closest('div')
        .querySelectorAll('.my-study-open');

      for (let myDetail of myDetailList) {
        myDetail.classList.add('my-study-detail-hidden');
      }

      for (let myOpen of myOpenList) {
        myOpen.classList.remove('d-none');
      }
      
      myStudyClose.classList.add('d-none');
    });
  }


  for (let myStudyOpen of myStudyOpenList) {
    myStudyOpen.addEventListener('click', function(event){
      event.preventDefault();

      for (let myStudyClose of myStudyCloseList) {
        myStudyClose.click();
      }

      let myDetailList = myStudyOpen
        .closest('td')
        .querySelectorAll('.my-study-detail');

      let myCloseList = myStudyOpen
        .closest('div')
        .querySelectorAll('.my-study-close');

      for (let myDetail of myDetailList) {
        myDetail.classList.remove('my-study-detail-hidden');
      }

      for (let myClose of myCloseList) {
        myClose.classList.remove('d-none');
      }

      myStudyOpen.classList.add('d-none');
    });
  }

}
