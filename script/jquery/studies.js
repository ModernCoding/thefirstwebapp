$(function() {

    // $('tr').on('click', function(){
    // 	console.log($(this).html());
    // });


    $('.my-content').each(function() {

        $(this).on('mouseover', function() {

            // $('#my-td').html('<span class="my-td-span">' +
            //     $(this).text() +
            //     '</span>'
            // );

            $('#my-td')
              .html(`<span class="my-td-span">${$(this).text()}</span>`);
            
        });

        $(this).on('mouseout', function() {
            $('#my-td').html(null);
        });

        /*
        $(this).on('click', function(e){

        	e.preventDefault();

        	var isHighlighted = $(this).hasClass('highlighted');

        	$('.highlighted').toggleClass('highlighted');

        	if (!isHighlighted){
        		$(this).addClass('highlighted');
        	}
        });
        */

    });


    $('.my-study-open').each(function() {

        $(this).on('click', function(e) {
            e.preventDefault();
            $(this).closest('td').find('.my-study-detail').removeClass('my-study-detail-hidden');
            $(this).closest('div').find('.my-study-close').removeClass('d-none');
            $(this).addClass('d-none');
        })

    });


    $('.my-study-close').each(function() {

        $(this).on('click', function(e) {
            e.preventDefault();
            $(this).closest('td').find('.my-study-detail').addClass('my-study-detail-hidden');
            $(this).closest('div').find('.my-study-open').removeClass('d-none');
            $(this).addClass('d-none');
        })

    });

});
