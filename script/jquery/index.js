$(function() {

    $('#button-inamori').on('click', function() {
        $(this).toggleClass('btn-secondary btn-light');
        $('#anchor-inamori').toggleClass('d-none');
        $('#inamori').toggleClass('d-none');
        resizeIframe();
    });

    $('#button-ma').on('click', function() {
        $(this).toggleClass('btn-secondary btn-warning');
        $('#anchor-ma').toggleClass('d-none');
        $('#ma').toggleClass('d-none');
        resizeIframe();
    });

    $('#button-bohlen').on('click', function() {
        $(this).toggleClass('btn-secondary btn-primary');
        $('#anchor-bohlen').toggleClass('d-none');
        $('#bohlen').toggleClass('d-none');
    });

    $('#button-nguyen').on('click', function() {
        $(this).toggleClass('btn-secondary btn-danger');
        $('#anchor-nguyen').toggleClass('d-none');
        $('#nguyen').toggleClass('d-none');
    });

});

window.onresize = function() {
    resizeIframe();
};

function resizeIframe() {
    for (let iframe of $('iframe')) {
        $(iframe).css('height', parseInt($(iframe).css('width'), 10) * 9 / 16);
    }
}