document.querySelector('body').addEventListener(
  'keyup',
  
  function(event){

    linkToIndex = document.querySelector('a[href="index.php"]');
    linkToStudies = document.querySelector('a[href="studies.html"]');
    linkToTop = document.querySelector('a[href="#top"]');

    buttonInamori = document.querySelector('#button-inamori');
    buttonMa = document.querySelector('#button-ma');
    buttonBohlen = document.querySelector('#button-bohlen');
    buttonNguyen = document.querySelector('#button-nguyen');

    aInamori = document.querySelector('a[href="#inamori"]');
    aMa = document.querySelector('a[href="#ma"]');
    aBohlen = document.querySelector('a[href="#bohlen"]');
    aNguyen = document.querySelector('a[href="#nguyen"]');

    switch (event.keyCode) {

      //  key 1
      case 49:

        if (buttonInamori !== null) {
          buttonInamori.click();
        }

        break;

        
      //  key 2
      case 50:
        
        if (buttonMa !== null) {
          buttonMa.click();
        }

        break;
        

      //  key 3
      case 51:

        if (buttonBohlen !== null) {
          buttonBohlen.click();
        }

        break;
      

      //  key 4
      case 52:

        if (buttonNguyen !== null) {
          buttonNguyen.click();
        }

        break;


      //  key 6
      case 54:

        if (aInamori !== null) {
          aInamori.click();
        }

        break;

        
      //  key 7
      case 55:
        
        if (aMa !== null) {
          aMa.click();
        }

        break;
        

      //  key 8
      case 56:

        if (aBohlen !== null) {
          aBohlen.click();
        }

        break;
      

      //  key 9
      case 57:

        if (aNguyen !== null) {
          aNguyen.click();
        }

        break;


      //  key I
      case 73:

        if (linkToIndex !== null) {
          linkToIndex.click();
        }

        break;

        
      //  key S
      case 83:

        if (linkToStudies !== null) {
          linkToStudies.click();
        }

        break;

        
      //  key T
      case 84:

        if (linkToTop !== null) {
          linkToTop.click();
        }

        break;


      default:
        break;
    }
  }
);
