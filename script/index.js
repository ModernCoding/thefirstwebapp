function resizeIframe() {
  for (let iframe of document.getElementsByTagName('iframe')) {
    iframe.style.height = `${iframe.offsetWidth * 9 / 16}px`;
  }
}


function resizeButtonPeople() {
  
  // resizing .button-people
  
  let width = 0;
  let buttons = document.getElementsByClassName('button-people');

  for (let button of buttons) {
    button.style.width = "auto";
    width = button.offsetWidth > width ? button.offsetWidth : width;
  }

  for (let button of buttons) {
    button.style.width = `${width + 1}px`;
  }


  // resizing .div-button-people > a
  
  width = 0;
  buttons = document.querySelectorAll('.div-button-people > a');

  for (let button of buttons) {
    button.style.width = "auto";
    width = button.offsetWidth > width ? button.offsetWidth : width;
  }

  for (let button of buttons) {
    button.style.width = `${width + 4}px`;
  }


  // resizing .div-button-people
  
  width = 0;
  buttons = document.getElementsByClassName('div-button-people');

  for (let button of buttons) {
    button.style.width = "auto";
    width = button.offsetWidth > width ? button.offsetWidth : width;
  }

  for (let button of buttons) {
    button.style.width = `${width + 9}px`;
  }
}


function stopAllMedias() {
  for (let audio of document.getElementsByTagName('audio')) {
    if (audio.classList.toString().indexOf('keep-playing') < 0) {
      audio.pause();
      audio.currentTime = 0;
    }
  }

  for (let video of document.getElementsByTagName('video')) {
    if (video.classList.toString().indexOf('keep-playing') < 0) {
      video.pause();
      video.currentTime = 0;
    }
  }

  for (let iframe of document.getElementsByTagName('iframe')) {
    if (iframe.classList.toString().indexOf('keep-playing') < 0) {
      iframe.setAttribute('src', iframe.getAttribute('src'));
    }
  }
}


window.onload = function() {

  resizeButtonPeople();
  
  window.onresize = function() {
    resizeIframe();
    resizeButtonPeople();
  };


  buttonClasses = [
    {'inamori': 'light'},
    {'ma': 'warning'},
    {'bohlen': 'primary'},
    {'nguyen': 'danger'}
  ];


  for (let buttonClass of buttonClasses) {
    for (let key in buttonClass) {
      document.getElementById(`button-${key}`).addEventListener(
        'click',
        
        function() {
          this.classList.toggle('btn-secondary');
          this.classList.toggle(`btn-${buttonClass[key]}`);
          
          document.getElementById(key).classList.toggle('d-none');
          
          document
            .getElementById(`anchor-${key}`)
            .classList
            .toggle('d-none');
          
          resizeIframe();
          resizeButtonPeople();
          stopAllMedias();
        }
      );
    }
  }

  // document.getElementById('button-ma').addEventListener(
  //   'click',
    
  //   function() {
  //     this.classList.toggle('btn-secondary');
  //     this.classList.toggle('btn-warning');
  //     document.getElementById('anchor-ma').classList.toggle('d-none');
  //     document.getElementById('ma').classList.toggle('d-none');
  //     resizeIframe();
  //   }
  // );


  for (let audio of document.getElementsByTagName('audio')) {
    audio.addEventListener('play', function() {
      this.classList.add('keep-playing');
      stopAllMedias();
      this.classList.remove('keep-playing');
    });
  }

  for (let video of document.getElementsByTagName('video')) {
    video.addEventListener('play', function() {
      this.classList.add('keep-playing');
      stopAllMedias();
      this.classList.remove('keep-playing');
    });
  }

  for (let iframe of document.getElementsByTagName('iframe')) {
    iframe.addEventListener('mouseenter', function() {
      this.classList.add('keep-playing');
      stopAllMedias();
      this.classList.remove('keep-playing');
    });
  }

}