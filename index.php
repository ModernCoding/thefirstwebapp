<?php //header('Location: /index.html'); ?>

<?php
  //  date in mm/dd/yyyy format;
  //  or it can be in other formats as well
  $birthDate = "09/28/1980";

  //  explode the date to get month, day and year
  $birthDate = explode("/", $birthDate);
  
  //  get age from date or birthdate
  $age = (
    date(
      "md",
      date(
        "U",
        mktime(
          0,
          0,
          0,
          $birthDate[0],
          $birthDate[1],
          $birthDate[2]
        )
      )
    ) > date("md")

      ? ((date("Y") - $birthDate[2]) - 1)
      : (date("Y") - $birthDate[2])
  );

  $dateArrivalPhnomPenh = explode("/", "07/09/2019");

  $arrivalDateMd = date(
      "md",
      date(
        "U",
        mktime(
          0,
          0,
          0,
          $dateArrivalPhnomPenh[0],
          $dateArrivalPhnomPenh[1],
          $dateArrivalPhnomPenh[2]
        )
      )
    );

  $phnomPenh = $arrivalDateMd > date("md")
    ? ((date("Y") - $dateArrivalPhnomPenh[2]) - 1)
    : (date("Y") - $dateArrivalPhnomPenh[2]);

  $phnomPenhUnit = $phnomPenh > 1 ? "years" : "year";

  if ($phnomPenh < 1) {
    $phnomPenh = $arrivalDateMd > date("md") ?
      date("m") - $dateArrivalPhnomPenh[0] + 12 :
      date("m") - $dateArrivalPhnomPenh[0];

    $phnomPenhUnit = $phnomPenh > 1 ? "months" : "month";
  } 
?>

<!DOCTYPE html>

<html>

  <head>
    <title>Michel Boretti</title>

    <meta
      charset="utf-8"
      name="viewport"
      content="
          width=device-width,
          initial-scale=1.0,
          maximum-scale=1.0,
          user-scalable=no
        "
    >

    <link
      rel="stylesheet"
      type="text/css"
      href="bootstrap/css/bootstrap.min.css"
    >
    
    <link
      rel="stylesheet"
      type="text/css"
      href="font-awesome/css/all.min.css"
    >

    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
  </head>


  <body>

    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
      <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbar-supported-content"
        aria-controls="navbar-supported-content"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>

      <div
        class="collapse navbar-collapse"
        id="navbar-supported-content"
      >
        <ul class="nav navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">About me</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="studies.html">My studies</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="me">
      <h1>Michel Boretti</h1>

      <h2>IT Engineer</h2>

      <figure>
        <img id="img-me" />
        <figcaption>Michel Boretti</figcaption>
      </figure>

      <p>
        My name is Michel Boretti, I am <?= $age ?> and I am from France. I am a developer.
      </p>

      <p>
        I studied Chemistry in France and Germany; more details about my studies <a href="studies.html">here</a>.
      </p>

      <p>
        As I came back to France, I decided to switch to a career in IT. I had the opportunity to enter an IT company and to be trained in the Mainframe technologies (COBOL, JCL, DB2, CICS) mostly used in the bank, finance and insurance industry. I have had the opportunity to work in France, Portugal and Spain.
      </p>

      <p>
        After seven years working in Europe, I got the opportunity to come to Vietnam and to work with .NET technologies. I have been staying during eight months in Saigon and three years in Danang.
      </p>

      <p>
        In 2017, I took the decision to switch to Ruby On Rails: technologies are always keeping changing and so should we do !
      </p>

      <p>
        In May 2019, I volunteered for Passerelles Numériques
        in Vietnam where I monitored two groups of students doing their final projects using PHP/Laravel and short after,
        I got the opportunity to come to Phnom Penh where
        I am now living since
        <?= $phnomPenh ?> <?= $phnomPenhUnit ?>.
      </p>

      <p id="top">
        People I admire are the following ones:
      </p>

      <!-- div class="row" id="people-menu -->
      <div class="div-button-people-list">

        <div class="div-button-people">
          <button
            id="button-inamori"
            class="btn btn-secondary button-people"
          >
            稲盛 和夫
          </button>
          
          <a
            id="anchor-inamori"
            href="#inamori"
            class="btn btn-light d-none"
            data-toggle="tooltip"
            data-placement="right"
            title="Get directly to biography !"
          >
            <i
              class="fas fa-fast-forward fa-rotate-90"
              aria-hidden="true"
            ></i>
          </a>
        </div>

        <div class="div-button-people">
          <button
            id="button-ma"
            class="btn btn-secondary button-people"
          >
            马云
          </button>
          
          <a
            id="anchor-ma"
            href="#ma"
            class="btn btn-warning d-none"
            data-toggle="tooltip"
            data-placement="right"
            title="Get directly to biography !"
          >
            <i
              class="fas fa-fast-forward fa-rotate-90"
              aria-hidden="true"
            ></i>
          </a>
        </div>

        <div class="div-button-people">
            <button id="button-bohlen" class="btn btn-secondary button-people">Dieter Bohlen</button>
            <a id="anchor-bohlen" href="#bohlen" class="btn btn-primary d-none" data-toggle="tooltip" data-placement="right" title="Get directly to biography !">
                <i class="fas fa-fast-forward fa-rotate-90" aria-hidden="true"></i>
            </a>
        </div>

        <div class="div-button-people">
          <button
            id="button-nguyen"
            class="btn btn-secondary button-people"
          >
            Nguyễn Thị Phương Thảo
          </button>
          
          <a
            id="anchor-nguyen"
            href="#nguyen"
            class="btn btn-danger d-none"
            data-toggle="tooltip"
            data-placement="right"
            title="Get directly to biography !"
          >
            <i
              class="fas fa-fast-forward fa-rotate-90"
              aria-hidden="true"
            ></i>
          </a>
        </div>

      </div>

    </div>


    <!--  People I admire -->


    <!--  稲盛 和夫 -->
    <div class="people d-none" id="inamori">
      <div class="name">
        <h2>
          <a
            href="https://ja.wikipedia.org/wiki/%E7%A8%B2%E7%9B%9B%E5%92%8C%E5%A4%AB"
            target="_blank"
          >
            稲盛 和夫
          </a>
        </h2>

        <figure>
          <img id="img-inamori" alt="稲盛 和夫">
          <figcaption>稲盛 和夫</figcaption>
        </figure>

        <a href="#top">Top</a>
      </div>

      <div class="biography">
        <p>
          稲盛 和夫（いなもり かずお、1932年（昭和7年）1月21日[2] - ）は、日本の実業家。京セラ・第二電電（現・KDDI）創業者。公益財団法人稲盛財団理事長。日本航空名誉会長。
        </p>

        <p>
          1932年（昭和7年）、鹿児島県鹿児島市薬師町に7人兄弟の二男として生まれる。父畩市は「稲盛調進堂」という名で印刷工場を経営していた。西田尋常高等小学校（現在の鹿児島市立西田小学校）[1]、鹿児島中学（現在の鹿児島高等学校）を卒業する。銀行就職を考えたが、周囲の勧めで鹿児島高等学校第三部（旧・県立高等女学校、現・鹿児島県立鶴丸高等学校）へ進学、その後鹿児島市立鹿児島玉龍高等学校に転校し同校の第一期生として、卒業する[要出典]。大阪大学医学部の受験に失敗し、当時新設大学であった鹿児島県立大学（現鹿児島大学）の工学部応用化学科で、有機化学を専攻[3]。
          1955年（昭和30年）、鹿児島県立大学工学部を卒業後、有機化学の教授の紹介でがいしメーカーの松風（しょうふう）工業に入社、1958年（昭和33年）退社する。妻の朝子は、禹長春の四女である[4]。
        </p>

        <iframe
          src="https://www.youtube.com/embed/-vDMpOZheXY"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>

      </div>

    </div>


    <!--  马云  -->
    <div class="people d-none" id="ma">
      <div class="name">
        <h2>
          <a
            href="https://zh.wikipedia.org/wiki/%E9%A9%AC%E4%BA%91" 
            target="_blank"
          >
            马云
          </a>
        </h2>

        <figure>
          <img id="img-ma" alt="马云">
          <figcaption>马云</figcaption>
        </figure>

        <a href="#top">Top</a>
      </div>

      <div class="biography">
        <p>
          马云 Jack Ma Enabling eCommerce- Small Enterprises, Global Players (39008130265) (cropped).jpg 出生 1964年9月10日（54歲） 中国浙江省杭州市 国籍 中国 别名 马云爸爸、马老师[1]、马云同志[2]、风清扬（阿里巴巴内部花名） 籍贯 中国浙江绍兴嵊州市（原嵊县）谷来镇 民族 汉族 语言 普通话、杭州话、英语 教育程度 本科 母校 杭州师范学院（现杭州师范大学） 职业 阿里巴巴集团董事局主席（董事长）[3]
          净资产 ▲ 400亿美元 (2018年) 政党 中国共产党 中国共产党[4][5] 配偶 张瑛 儿女 儿子：马元坤 1992年（26－27歲） 女儿：马元宝 网站 马云个人新浪博客 马云个人新浪微博 马云（英语：Jack Ma，1964年9月10日－）[6]，祖籍浙江省绍兴嵊州市（原嵊县）谷来镇，后父母移居杭州，出生于浙江省杭州市，中国企业家，中国共产党党员。曾为亚洲首富，阿里巴巴集团董事局主席（董事长）[7]，淘宝网、支付宝的创始人和日本軟銀董事，大自然保護協會中國理事會主席兼全球董事會成員，華誼兄弟董事。
        </p>

        <p>
          馬雲本人的簽名是繁体字的「馬雲」[8]。馬雲與馬化騰在網路服務業並稱為「二馬」。騰訊有本內刊，叫做《騰雲》，網路服務業界曾就著刊名和兩人的名字開玩笑。圈內有人打趣說，馬化騰是希望排在馬雲前面。[9]
        </p>

        <h3>求學生涯</h3>

        <p>
          马云幼年时就读于杭州市中北二小[10][11]（现长寿桥小学）。后在杭州市天水中学短期就读[12]。1982年，马云高中毕业后，参加第一次高考，数学考了1分。1983年，第二次参加高考，还是落榜，数学考了19分。[13]因此经過父親拜託，開始替山海经、东海、江南三家杂志社踩三轮車送書，白天上班，晚上上夜校。1984年，第三次参加高考，数学考了89分，终于考上了杭州师范学院外语系外语外贸专业的专科。1988年毕业于杭州师范学院（现杭州师范大学）外语系英语专业[7]，获英语学士学位，之后于杭州电子工业学院（现杭州电子科技大学）任英文及国际贸易讲师[14]。
        </p>

        <h3>創業历程</h3>

        <p>
          1991年，马云初次接触商业活动，集资3,000人民币创办海博翻译社。头一个月，收入人民幣700元，但房租高达人民幣1,500元。他于是利用转手小商品交易的方式，从广州、义乌等地进货，成功养活了翻译社，还组织了杭州第一个英语角。[15]1995年，马云在出访美国时首次接触到因特网，回国后和他的妻子，还有同为老师的何一兵于1995年4月创办网站“中国黄页”，专为中国公司制作网页，其后不到三年时间，他们利用该网站赚到了人民幣500万元[7]。1997年，他为中国外经贸部开发其官方站点及中国产品网上交易市场。[15]
          1999年，马云正式辞去公职，创办阿里巴巴网站，[7]开拓电子商务应用，尤其是B2B业务。2003年秘密成立淘宝網，2004年创立独立的第三方电子支付平台支付宝，兩者目前在中國市場都處於領先地位，同時，阿里巴巴是全球領先的B2B網站。2006年马云成為央視二套《贏在中國》評委，還用中國雅虎和阿里巴巴為《贏在中國》官方網站提供平台。2013年5月10日，在淘宝十周年晚会上宣布，马云卸任阿里巴巴集团首席执行官（CEO）一职，由陆兆禧接任[16]。 2018年8月29日，支付宝（中国）信息技术有限公司法人发生变更，由马云变更为叶郁青[17]；2018年9月10日，马云宣布2018年9月10日起，他将不再担任集团董事局主席（董事长），届时由张勇接任。[18][19]
        </p>

        <h3>公益事业</h3>

        <p>
          2015年4月3日，马云向母校杭州师范大学捐赠1亿元人民币，设立“杭州师范大学马云教育基金”，主要用于资助教育研究与教育创新。[20]2015年4月，马云与马化腾等人联合发起成立公益机构“桃花源生态保护基金会”，从事环保公益事业。他与中国艺术家曾梵志联合创作油画《桃花源》，并将油画拍卖所得全部捐给桃花源生态保护基金会。
        </p>

        <iframe
          src="https://www.youtube.com/embed/WsQ7ysVt-0A"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>

      </div>

    </div>


    <!--  Dieter Bohlen -->
    <div class="people d-none" id="bohlen">

      <div class="name">
        <h2>
          <a
            href="https://de.wikipedia.org/wiki/Dieter_Bohlen"
            target="_blank"
          >
            Dieter Bohlen
          </a>
        </h2>

        <figure>
          <img id="img-bohlen" alt="Dieter Bohlen">
          <figcaption>Dieter Bohlen</figcaption>
        </figure>

        <a href="#top">Top</a>
      </div>

      <div class="biography">
        <p>
          Dieter Günter Bohlen (* 7. Februar 1954 in Berne) ist ein deutscher Musiker, Produzent und Songwriter. Er wurde in den 1980er-Jahren als Mitglied des Pop-Duos Modern Talking bekannt. Neben erfolgreichen Produktionen nationaler und internationaler Künstler
          ist er ständiges Jurymitglied der Casting-Sendungen Deutschland sucht den Superstar und Das Supertalent.
        </p>

        <h3>Kindheit und Jugend</h3>

        <p>
          Bohlen wuchs bei seinen Eltern Hans (* 1928) und Edith Bohlen (* 1936) in Ostfriesland auf. Später zog die Familie in den Oldenburger Stadtteil Eversten. Bohlen war in seiner Jugend eine Zeit lang Mitglied der SDAJ und kurzzeitig Mitglied der Deutschen
          Kommunistischen Partei, ist aber heute parteilos.[1][2] Nach dem Abitur am Wirtschaftsgymnasium der Berufsbildenden Schulen in Oldenburg-Haarentor zog Bohlen nach Göttingen. Dort studierte er an der Georg-August-Universität auf Wunsch
          seiner Eltern Betriebswirtschaftslehre. Sein Studium schloss er 1978 als Diplom-Kaufmann ab.[3]
        </p>

        <h3>Musikalische Anfänge</h3>

        <p>
          Bereits während seiner Schulzeit komponierte Bohlen Musik. Ab Ende der 1970er Jahre war er beim Hamburger Musikverlag Intersong als Komponist und Songschreiber für zahlreiche Schlagersänger tätig. Zusammen mit Holger Garbode gründete er das Duo Monza,
          für das Tony Hendrik 1978 die Single Hallo Taxi Nummer 10 produzierte und komponierte. Es war Bohlens erste Schallplatte, und sie blieb ohne Resonanz. Ab 1980 arbeitete er als Produzent für die Berliner Schallplattenfirma Hansa und als
          Solokünstler unter dem Pseudonym „Steve Benson“. Die erste Single hier hieß Don’t Throw My Love Away (1980). Es folgten zwei weitere Singles mit den Titeln Love Takes Time und (You’re a Devil With) Angel Blue Eyes. Keine dieser Platten
          schaffte es in die Top 100, was 1981 zur Auflösung des Projekts führte. Im gleichen Jahr trat er der Formation Sunday bei und hatte mit ihr und Halé, Hey Louise Anfang 1982 einen Auftritt in der ZDF-Hitparade. 1983 nahm er erstmals an
          der Vorentscheidung zum Grand Prix Eurovision de la Chanson teil. Bernd Clüver sang den von Bohlen komponierten Titel Mit 17 und belegte damit in der Vorentscheidung den dritten Platz.
        </p>

        <h3>Erfolge als Produzent</h3>

        <p>
          Nachdem Bohlen mit dem Schlagersänger Thomas Anders ab 1982 ohne Erfolg sechs Singles mit deutschen Texten veröffentlicht hatte, produzierte er ab Herbst 1984 das Bohlen/Anders-Duo Modern Talking. Die Gruppe belegte mit You’re My Heart, You’re My Soul,
          You Can Win If You Want, Cheri Cheri Lady, Brother Louie und Atlantis Is Calling (S.O.S. for Love) fünf Mal Platz 1 der deutschen Single-Charts und war auch in anderen europäischen sowie in asiatischen und afrikanischen Hitparaden erfolgreich.
          1987 trennten sich Modern Talking im Streit. Bohlen produzierte und schrieb danach für andere Interpreten, unter anderem für die von ihm entdeckte C. C. Catch (Heartbreak Hotel). Unter dem Namen Blue System (Under My Skin) trat er weiterhin
          selbst auf. 1998 fanden Modern Talking wieder zusammen, trennten sich aber 2003 erneut. 1986 schrieb Bohlen für die Tatort-Episode Der Tausch den Song Midnight Lady, den der ehemalige Sänger von Smokie, Chris Norman sang. Der Titel wurde
          ebenfalls ein Nummer-eins-Hit. In der Tatort-Folge Moltke (mit Götz George als Schimanski und Eberhard Feik als Kommissar Thanner) stammte der Titelsong Silent Water ebenfalls von Bohlen. 1989 gewann er mit seinen Liedern die Grand-Prix-Vorentscheidungen
          in Deutschland und Österreich. Nino de Angelo trat für Deutschland mit dem Lied Flieger an und belegte beim Grand Prix Platz 14. Thomas Forstner trat mit dem Titel Nur ein Lied für Österreich an und erreichte den fünften Platz. 1992 trat
          Tony Wegas für Österreich mit dem von Bohlen komponierten Song Zusammen geh’n beim Grand Prix an und belegte den zehnten Rang. 1997 beendete Bohlen sein Projekt Blue System, nachdem dessen letzte Veröffentlichungen in den Medien und vom
          Publikum nur noch sehr zurückhaltend wahrgenommen wurden. Im März 2006 erschien ein neues Album von Bohlen, das neben zwölf neuen und sechs alten Titeln auch die letzte nicht veröffentlichte Modern-Talking-Single Shooting Star und den
          Titelsong aus Dieter – Der Film, Gasoline, enthält. 2006 trat Bohlen bei der rumänischen Vorentscheidung zum Eurovision Song Contest 2006 als Produzent des Duos Indiggo mit dem Stück Be My Boyfriend an; es erreichte bei der Vorentscheidung
          den siebten Platz. 2010 erreichte das von Bohlen für die Sängerin Andrea Berg produzierte Album Schwerelos Platz 1 der deutschen Albumcharts und wurde mit Dreifach-Platin ausgezeichnet.[4] Das zweite von Bohlen für Berg produzierte Album
          Abenteuer konnte dieses 2011 ebenfalls erreichen. Um sich vor einer Zuordnung zu einer bestimmten Musikstilrichtung zu schützen, verwendete Bohlen, meist als Produzent, immer wieder wechselnde Pseudonyme, wie Art of Music, Dee Bass, Fabricio/Fabrizio
          Bastino, Howard Houston, Jennifer Blake, Joseph Cooley, Marcel Mardello, Steve Benson, Ryan Simmons, Barry Mason, Michael von Drouffelaar, Atisha, David Bernhardt oder Projekte wie Mayfair, Monza, Foolish Heart, Countdown G.T.O., Hit the
          Floor oder Major T.[5][6][7][8]
        </p>

        <h3>Privatleben</h3>

        <p>
          1983 heirateten Bohlen und seine langjährige Freundin Erika Sauerland, mit der er drei Kinder hat.[9] 1989 trennte sich das Ehepaar, und Bohlen begann eine Beziehung mit Nadja Abd el Farrag.[10] 1996 trennte sich das Paar, und Bohlen heiratete Verona
          Feldbusch. Die Ehe hielt nur einen Monat, und Feldbusch gab als Scheidungsgrund an, von Bohlen geschlagen worden zu sein, was dieser bestreitet.[11] Von 1997 bis 2000 war er erneut mit Abd el Farrag liiert.[11] Von 2001 bis August 2006
          lebte er mit Estefania Küster zusammen, mit der er einen Sohn hat.[12] Seit Herbst 2006 ist er mit Fatma Carina Walz (* 1984) liiert, mit der er eine Tochter (* 2011) und einen Sohn (* 2013) hat.[13] Dieter Bohlen wird 2018 vom Manager
          Magazin mit einem geschätzten Vermögen von 250 Millionen Euro auf Platz 624 der Liste der reichsten Deutschen geführt.[14]
        </p>

        <audio controls>
          <source src="audio/blue_system.mp3" type="audio/mp3">
        </audio>
      </div>

    </div>


    <!--  Nguyễn Thị Phương Thảo  -->
    <div class="people d-none" id="nguyen">
      <div class="name">
        <h2>
          <a
            href="https://vi.wikipedia.org/wiki/Nguy%E1%BB%85n_Th%E1%BB%8B_Ph%C6%B0%C6%A1ng_Th%E1%BA%A3o" 
            target="_blank"
          >
            Nguyễn Thị Phương Thảo
          </a>
        </h2>

        <figure>
          <img id="img-nguyen" alt="Nguyễn Thị Phương Thảo">
          <figcaption>Nguyễn Thị Phương Thảo</figcaption>
        </figure>

        <a href="#top">Top</a>
      </div>

      <div class="biography">
        <p>
          Nguyễn Thị Phương Thảo sinh năm 1970 tại Hà Nội, là một nữ doanh nhân, tỷ phú hiện trên cương vị là tổng giám đốc của VietJet Air, Phó chủ tịch thường trực Hội đồng quản trị Ngân hàng HDBank.[1][2] Bà là người Việt Nam thứ 2 được Forbes ghi nhận là tỉ
          phú USD, sau Phạm Nhật Vượng.[3]
        </p>

        <p>
          Theo Hãng tin Bloomberg, Nguyễn Thị Phương Thảo kiếm được 1 triệu USD đầu tiên khi mới chỉ 21 tuổi, nhờ bán máy fax và nhựa cao su.[4] Sau khi quay về Việt Nam, bà góp vốn thành lập Ngân hàng Techcombank và sau đó là VIB - 2 trong số những ngân hàng tư
          nhân đầu tiên ở Việt Nam. Gần 25 năm sau, bà nổi lên như một nữ tỷ phú đôla đầu tiên của Việt Nam. Phần lớn tài sản của bà đến từ cổ phần ở VietJet và Dragon City (Phú Long) - dự án bất động sản rộng 65 héc-ta ở TP. HCM.[4] Tháng 9/2013,
          hai vợ chồng bà được báo chí và dư luận quan tâm, sau khi có thông tin VietJet Air của họ đặt mua 100 máy bay Airbus trị giá 9,1 tỷ USD.[1] Ngày 23/5/2016, dưới sự chứng kiến của Tổng thống Mỹ Barack Obama và Chủ tịch nước Trần Đại Quang,
          hãng hàng không VietJet đã ký thỏa thuận thuê mua 100 chiếc Boeing 737 MAX 200 của tập đoàn đến từ nước Mỹ trị giá 11,3 tỷ USD.[5]
        </p>

        <h3>Khu nghỉ dưỡng</h3>

        <p>
          Ngoài việc là cổ đông lớn nhất của VietJet Air, Tập đoàn Sovico Holdings của gia đình bà đã mua lại Furama Resort Danang vào năm 2005, trở thành nhà đầu tư người Việt đầu tiên sở hữu và vận hành khách sạn 5 sao. Furama Resort Danang khai trương vào năm
          1997 với 198 phòng là khu nghỉ dưỡng biển 5 sao đầu tiên tại Việt Nam. Gần một thập kỷ sau đó, Sovico tiếp tục thâu tóm thêm 2 khu nghỉ dưỡng tại Khánh Hoà là Ana Mandara và An Lâm Ninh Vân Bay.[6]
        </p>

        <h3>Tỷ phú Việt Nam</h3>

        <p>
          Ngày 9/3/2017, tạp chí Forbes công bố danh sách các nữ tỷ phú USD trên thế giới năm 2017, ghi nhận bà Phương Thảo là nữ tỷ phú đầu tiên của Việt Nam và khu vực Đông Nam Á, với khối tài sản ước tính khoảng 1,7 tỷ USD.[7] Bà cũng là một trong 15 nữ tỷ phú
          tự thân mới trong danh sách của Forbes năm 2017.
        </p>

        <h3>Tài sản hiện tại và thành tựu</h3>

        <p>
          Tính đến ngày 7/4/2018, tài sản của bà đã tăng lên $3,7 tỷ.
        </p>

        <p>
          <video controls>
            <source src="video/nguyen.mp4" type="video/mp4">
          </video>
        </p>
      </div>

    </div>

    <script
      type="text/javascript"
      src="script/jquery/jquery.min.js"
    ></script>
    
    <script
      type="text/javascript"
      src="bootstrap/js/bootstrap.bundle.min.js"
    ></script>
    
    <script type="text/javascript" src="script/app.js"></script>
    
    <script
      type="text/javascript"
      src="script/jquery/app.js"
    ></script>
    
    <!-- <script
      type="text/javascript"
      src="script/jquery/index.js"
    ></script> -->
    
    <script type="text/javascript" src="script/index.js"></script>

  </body>

</html>
